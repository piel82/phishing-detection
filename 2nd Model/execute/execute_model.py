import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.metrics import confusion_matrix
from tensorflow import keras
import html2text
import pickle


tokenizer_path = "tokenizer.pickle"

model_path = "my_model_2"

print("Enter Filepath to .txt File")
data_path = input()

# loading Model and tokenizer
with open(tokenizer_path, 'rb') as handle:
    tokenizer = pickle.load(handle)
loaded_model = keras.models.load_model(model_path)

vocab_size = 2000
embedding_dim = 16
max_length = 1000
trunc_type='post'
padding_type='post'
oov_tok = "<OOV>"

def myPred(text):
    text = [text]
    sequences = tokenizer.texts_to_sequences(text)
    padded = pad_sequences(sequences,maxlen=max_length, padding=padding_type, 
                       truncating=trunc_type)

    return loaded_model.predict(padded)

def f(x):
    if x>=0.7:
        return 1
    else:
        return 0

#Converter for the emails from Oliver
def textFromOliverMail(text):
    text = text[text.find("Subject: "):]
    subject = text[:text.find("\n") + 1]

    text = text[text.find("Date: "):]
    text = text[text.find("\n"):]

    text = subject + text

    h = html2text.HTML2Text()
    h.ignore_links = True
    text = h.handle(text)
    return text

output = open("Prediction.txt", "a")
textArr = ((open(data_path, 'rb').read()).decode('utf-8', 'ignore')).split("X-CCC-HASH:")[1:]
for i in range(len(textArr)):
    try:
        textArr[i] = textFromOliverMail(textArr[i])
        pred = str(myPred(textArr[i])) + "; " + str(f(myPred(textArr[i])))
        output.write(pred)
        print(myPred(textArr[i]), f(myPred(textArr[i])))
    except:
        continue

output.close()