import PythonKit
PythonLibrary.useVersion(3, 8)
import Foundation

let tf: PythonObject = Python.import("tensorflow")
let keras: PythonObject = Python.import("tensorflow.keras")
let np: PythonObject = Python.import("numpy")

let vocabSize: Int = 1000 //Words that the model accepts
let embeddingDim: Int = 64 //Dimensionaity of the embedding
let alpha: Float = 0.0004 //Leraning rate

let token_start: Int = vocabSize + 0
let token_end: Int = vocabSize + 1
let token_unknown: Int = vocabSize + 2
let token_padding: Int = vocabSize + 3

let inputLength: Int = 50

// filename for saved model which should be loaded
let load_model_path: String = "/home/pierre/Documents/Uni/softwareprojekt_krypto/phishing-detection/swift_port/Sources/swift_port/checkpoints/model.05-0.2102.h5"
// filename for file which contains most frequent words ordered descending after frequency 
let filename_load_words: String = "/home/pierre/Documents/Uni/softwareprojekt_krypto/phishing-detection/swift_port/Sources/swift_port/words1000.csv"
// create dictionary with words as keys and their frequency in the emails as value. we load this from a file
var text: String = try String(contentsOfFile: filename_load_words, encoding: .utf8)
let wordsArr: [String] = text.components(separatedBy: " ")

var w : String = wordsArr[0]
var wordsArrSeperated: [String] = text.components(separatedBy: "\r\n")

var wordPositions: [String: Int] = [:]
var i: Int = 1

for key in wordsArrSeperated {
    wordPositions[key] = i
    i += 1
    //print(key)
}

// convert an email given as a string into an array of words. the words are sanitized
func getTextArr(text: String) -> [String] {
    let lowercase_text = text.lowercased()
    var sanitized_text: String = ""
    let replace_characters: [String] = [",", "!", "?", "."]
    let digits: [String] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

    // filter invalid letter out of the text
    for letter in lowercase_text {
        let str: String = String(letter)
        if replace_characters.contains(str) {
            sanitized_text += " " + str + " "
        }
        else if digits.contains(str) {
            sanitized_text += "[DIGIT]"
        }
        else if("a" <= letter && letter <= "z" || "A" <= letter && letter <= "Z" || letter == " ") {
            sanitized_text += str
        }
        else {
            sanitized_text += " "
        }
    }
    if sanitized_text.hasPrefix("subject: ") {
        sanitized_text = String(sanitized_text.suffix(9)) // remove "subject: " from the beginning
    }
    //print(sanitized_text)
    return sanitized_text.components(separatedBy: " ")
}

// convert an email given as array of sanitized words into an array of integers as input for the neural net
func textToIntArr(text: String) -> [Int] {
    let textArr: [String] = getTextArr(text: text)
    var converted: [Int] = Array(repeating: token_padding, count: inputLength)
    converted[inputLength - 1] = token_end
    var counter = 0
    converted[max(1, inputLength - 1 - textArr.count) - 1] = token_start

    for i in max(1, inputLength - 1 - textArr.count)..<inputLength - 1 {
        if (textArr[counter] == "enron") {
            converted[i] = token_unknown
        }
        else if let val = wordPositions[textArr[counter]] {
            converted[i] = val
        }
        else {
            converted[i] = token_unknown
        }
        counter += 1
    }
    //print(converted)

    return converted
    /*var textInts: [Int]
    if (converted.count >= inputLength - 2) {
        textInts = [token_start] + converted + [token_end]
    }
    else {
        textInts = [token_start] + Array(repeating: token_padding, count: inputLength - converted.count - 2) + converted + [token_end]
    }
    return textInts*/
}


let model: PythonObject = keras.models.load_model(load_model_path)


// Testing
let userText: [String] = [
    "Dear tim, I’m sorry for the unpleasant experience you had in our store and I can understand your frustration. I have forwarded your complaint to our management team, and we’ll do our best to make sure this never happens again. I refunded your purchase, and your funds should be with you shortly. We also want to offer you a 10% discount for your next purchase in our store. Please use this promo code to get a discount: [link]. Please accept our apologies for the inconvenience you had. Best regards,",
    "get rich fast Dragon bitcoin now free money for you click on the link and get rich today. earn 1000$ a day from home without any problem.",
    "account security Dear amazon user, your account has been hacked, click on the link below to verify your account."
]


var modelInput = Array(repeating: Array(repeating: Array(repeating: 0, count: 1), count: inputLength), count: 3)

for i in 0...2 {
    let arr: [Int] = textToIntArr(text: userText[i])
    for j in 0..<inputLength {
        modelInput[i][j][0] = arr[j]
    }
}

/*
modelInput[0, :, 0] = np.array(textToIntArr(userText[0]))
modelInput[1, :, 0] = np.array(textToIntArr(userText[1]))
modelInput[2, :, 0] = np.array(textToIntArr(userText[2]))
*/

let numpy_model_input = np.array(modelInput)
let result = model.predict(modelInput)

print(result)