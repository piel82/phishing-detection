import tensorflow as tf
from tensorflow import keras
import datetime
import random
import numpy as np
from numpy import genfromtxt
import os
import time
from collections import Counter
import re
import html2text
import matplotlib.pyplot as plt

#I have problems with tensorflow on windows, next 4 lines may not be needed
configproto = tf.compat.v1.ConfigProto() 
configproto.gpu_options.allow_growth = True
sess = tf.compat.v1.Session(config=configproto) 
tf.compat.v1.keras.backend.set_session(sess)

inputLength = 80 #First inputLength words
vocabSize = 1000 #Words that the model accepts

token_start = vocabSize + 0
token_end = vocabSize + 1
token_unknown = vocabSize + 2
token_padding = vocabSize + 3

#Path to load model
loadModelPath = 'models\\m12'

#Converts a text to an int array with the corresponding positions in the frequency matrix
def textToIntArr(text):
    textArr = getTextArr(text)
    textInts = [token_start] + [wordPositions[w] if w in wordPositions and w != 'enron' else token_unknown for w in textArr[:inputLength - 2]] + [token_end]
    if len(textInts) < inputLength:
        textInts = [token_padding] * (inputLength - len(textInts)) + textInts
    return textInts

#Split text by words and special chars
def getTextArr(text):
    text = text.lower()
    text = text.replace(".", " . ")
    text = text.replace(",", " , ")
    text = text.replace("!", " ! ")
    text = text.replace("?", " ? ")


    if text.startswith("subject: "):
        text = text[len("subject: "):]

    text = re.sub('[^0-9a-zA-Z\s.,!?]+', ' ', text)

    #Replace digits with a token, so the model still differentiates between numbers of different length
    text = re.sub('\d', '[DIGIT]', text)
    return text.split()

#Converter for the emails from Oliver
def textFromOliverMail(text):
    text = text[text.find("Subject: "):]
    subject = text[:text.find("\n") + 1]

    text = text[text.find("Date: "):]
    text = text[text.find("\n"):]

    text = subject + text

    try:
        h = html2text.HTML2Text()
        h.ignore_links = True
        text = h.handle(text)
    except:
        text = None


    return text

wordsArr = None
text = open('words1000.csv').read()
wordsArr = text.split()
wordPositions = {wordsArr[i] : i for i in range(len(wordsArr))} #Dict for retrieveing postion for each word

model = tf.keras.models.load_model(loadModelPath)


files = ["year" + str(i) + ".txt" for i in range(2006, 2019)]

#For backtesting the model
#Run some tests
for f in files:
    #Email from Olivers dataset
    print("File " + f)
    textArr = ((open(f, 'rb').read()).decode('utf-8', 'ignore')).split("X-CCC-HASH:")[1:]

    parsedMails = []
    for i in range(len(textArr)):
        parsed = textFromOliverMail(textArr[i])
        if parsed != None:
            parsedMails.append(parsed)

    modelInput = np.empty((len(parsedMails), inputLength, 1))

    for i in range(len(parsedMails)):
        modelInput[i, :, 0] = textToIntArr(parsedMails[i])

    result = model.predict(modelInput)
    np.savetxt("result.csv", result)

    hamCount = 0
    spamCount = 0

    threshold = 0.5
    for i in range(len(result)):
        if result[i][0] > threshold:
            spamCount += 1
        else:
            hamCount += 1


    print("Ham: " + str(hamCount))
    print("Spam: " + str(spamCount))

    print("Enter for next file")
    plt.plot(result)
    plt.show()
    input()

print("Enter to exit")
input()