import tensorflow as tf
from tensorflow import keras
import datetime
import random
import numpy as np
from numpy import genfromtxt
import os
import time
from collections import Counter
import re
import html2text

#I have problems with tensorflow on windows, next 4 lines may not be needed
configproto = tf.compat.v1.ConfigProto() 
configproto.gpu_options.allow_growth = True
sess = tf.compat.v1.Session(config=configproto) 
tf.compat.v1.keras.backend.set_session(sess)

loadModel = True
trainModel = False
readWords = True
onlyBenchmark = True

epochs = 0
inputLength = 80 #First inputLength words

vocabSize = 1000 #Words that the model accepts
embeddingDim = 64 #Dimensionaity of the embedding
alpha = 0.0004 #Leraning rate

token_start = vocabSize + 0
token_end = vocabSize + 1
token_unknown = vocabSize + 2
token_padding = vocabSize + 3

#Paths to load and save models
#loadModelPath = 'checkpoints\\model.06-0.0856.h5'
loadModelPath = 'models\\m12'
saveModelPath = 'models\\m13'

#The model seems to have a bug as the accuracy is way too high

dirHam = None
dirSpam = None

if onlyBenchmark == False:
    dirHam = os.listdir('D:\Enron Emails\enron\ham')
    dirSpam = os.listdir('D:\Enron Emails\enron\spam')

    hamCount = len(dirHam)
    spamCount = len(dirSpam)

#print("Ham count:")
#print(hamCount)
#print("Spam count:")
#print(spamCount)

wordFreqs = Counter()

#Converts a text to an int array with the corresponding positions in the frequency matrix
def textToIntArr(text):
    textArr = getTextArr(text)
    textInts = [token_start] + [wordPositions[w] if w in wordPositions and w != 'enron' else token_unknown for w in textArr[:inputLength - 2]] + [token_end]
    if len(textInts) < inputLength:
        textInts = [token_padding] * (inputLength - len(textInts)) + textInts
    return textInts

#Split text by words and special chars
def getTextArr(text):
    text = text.lower()
    text = text.replace(".", " . ")
    text = text.replace(",", " , ")
    text = text.replace("!", " ! ")
    text = text.replace("?", " ? ")


    if text.startswith("subject: "):
        text = text[len("subject: "):]

    text = re.sub('[^0-9a-zA-Z\s.,!?]+', ' ', text)

    #Replace digits with a token, so the model still differentiates between numbers of different length
    text = re.sub('\d', '[DIGIT]', text)
    return text.split()

#Converter for the emails from Oliver
def textFromOliverMail(text):
    text = text[text.find("Subject: "):]
    subject = text[:text.find("\n") + 1]

    text = text[text.find("Date: "):]
    text = text[text.find("\n"):]

    text = subject + text

    h = html2text.HTML2Text()
    h.ignore_links = True
    text = h.handle(text)
    return text

wordsArr = None

#If readWords is True, load the words from a csv
if readWords:
    text = open('words1000.csv').read()
    wordsArr = text.split()
    #print(wordsArr)
    #print(len(wordsArr))
    #input()
else:
    #Count word freqencies in ham and spam mails
    for i in range(hamCount):
        file = dirHam[i]
        text = (open('D:\Enron Emails\enron\ham\\' + file, 'rb').read()).decode('utf-8', 'ignore')

        textArr = getTextArr(text) #Replace Satzzeichen mit Leerzeichen davor

        freq = Counter(textArr)

        wordFreqs += freq
        if i % 100 == 0:
            print(i/hamCount)

    for i in range(spamCount):
        file = dirSpam[i]
        text = (open('D:\Enron Emails\enron\spam\\' + file, 'rb').read()).decode('utf-8', 'ignore')

        textArr = getTextArr(text)

        freq = Counter(textArr)

        wordFreqs += freq
        if i % 100 == 0:
            print(i/spamCount)

    wordFreqsDict = dict(wordFreqs)

    wordsArr = sorted(wordFreqsDict, key=wordFreqsDict.get, reverse=True) #Array of words ordered by frequencies
    wordsArr = wordsArr[:vocabSize] #Cut off after vocabSize words
    npWordsArr = np.array(wordsArr)
    np.savetxt("words1000_2.csv", npWordsArr, fmt='%s')

wordPositions = {wordsArr[i] : i for i in range(len(wordsArr))} #Dict for retrieveing postion for each word
#print(wordPositions)

if onlyBenchmark == False:

    #Prepare the data containers that go into the model
    dataX = np.empty((hamCount + spamCount, inputLength, 1))
    dataY = np.empty((hamCount + spamCount, 1))

    #Shuffle so the mails are ordered by ham or spam
    indexList = [i for i in range(hamCount + spamCount)]
    random.shuffle(indexList)



    for i in range(hamCount):
        file = dirHam[i]
        text = (open('D:\Enron Emails\enron\ham\\' + file, 'rb').read()).decode('utf-8', 'ignore')

        dataX[indexList[i], :, 0] = textToIntArr(text)
        dataY[indexList[i], 0] = 0

    for i in range(spamCount):
        file = dirSpam[i]
        text = (open('D:\Enron Emails\enron\spam\\' + file, 'rb').read()).decode('utf-8', 'ignore')

        dataX[indexList[hamCount + i], :, 0] = textToIntArr(text)
        dataY[indexList[hamCount + i], 0] = 1


    print(dataX)
    print(dataY)
    input()


model = None
if (loadModel):
    model = tf.keras.models.load_model(loadModelPath)
else:
    #Create model
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Embedding(vocabSize + 4, embeddingDim))
    model.add(tf.keras.layers.LSTM(128, return_sequences=True))
    model.add(tf.keras.layers.LSTM(128))
    model.add(tf.keras.layers.Dense(140, activation="relu"))
    model.add(tf.keras.layers.Dense(1, activation="sigmoid"))

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=alpha),
                    loss='binary_crossentropy', 
                    metrics=['accuracy', tf.keras.metrics.FalsePositives(),  tf.keras.metrics.FalseNegatives()])

    model.summary()


if onlyBenchmark == False:
    if (trainModel):
        log_dir = "logs\\fit\\" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

        checkpoint_path = "checkpoints\\"# + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        cp_callback = tf.keras.callbacks.ModelCheckpoint(
                        filepath=checkpoint_path + 'model.{epoch:02d}-{val_loss:.4f}.h5', 
                        verbose=1, 
                        save_weights_only=False,
                        save_freq='epoch')

        model.fit(x=dataX, 
                  y=dataY,
                  shuffle=True,
                  epochs=epochs,
                  batch_size=128,
                  validation_split=0.1,
                  verbose=2,
                  callbacks=[tensorboard_callback, cp_callback])

        model.save(saveModelPath)

#For backtesting the model
#Run some tests
while 1:
    #Email from Olivers dataset
    print("Enter a Filepath")
    filePath = input()
    textArr = ((open(filePath, 'rb').read()).decode('utf-8', 'ignore')).split("X-CCC-HASH:")[1:]


    modelInput = np.empty((len(textArr), inputLength, 1))

    for i in range(len(textArr)):
        textArr[i] = textFromOliverMail(textArr[i])
        modelInput[i, :, 0] = textToIntArr(textArr[i])



    result = model.predict(modelInput)


    for i in range(len(textArr)):
        print("Mail " + str(i))
        print("Spam/ phishing probability: ")
        print(textArr[i])
        print(result[i])
        print("####################")
        input()

